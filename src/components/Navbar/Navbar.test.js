import React from 'react';
import { shallow } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Navbar from './Navbar';

configure({ adapter: new Adapter() });

describe('Navbar test', () => {
  it('Render without crash', () => {
    const navbar = shallow(<Navbar />);
    expect(navbar);
  });
});
