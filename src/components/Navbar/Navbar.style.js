import styled from 'styled-components';

export const Box = styled.div`
  height: 120px;
  background-image: radial-gradient(
    #d7d7d7,
    #b5b5b5,
    #8a8a8a,
    #666666,
    #414141
  );
  border-style: solid;
  border-radius: 10px;
`;

export const Container = styled.div`
  padding-right: 15px;
  padding-left: 15px;
`;

export const Brand = styled.a`
  float: left;
  height: 20px;
  font-size: 18px;
  line-height: 20px;
  margin-top: -40px;
`;

export const SearchField = styled.div`
  padding: 12px 15px;
  margin-right: 15px;
`;

export const SearchInput = styled.input`
  height: 50px;
  width: 400px;
  float: right !important;
  margin-right: -15px;
  margin-top: 15px;
  font-size: 16pt;
  border-style: ridge;
  border-radius: 5px;
  border-color: #cc3300;
  box-shadow: 3px 3px 5px #182019;
`;

export const Image = styled.img`
  margin-left: 50px;
`;
