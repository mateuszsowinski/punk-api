import React, { Component } from 'react';
import PubSub from 'pubsub-js';
import { Link } from 'react-router-dom';
import Logo from '../../assets/Logo.png';
import {
  Box,
  Container,
  Brand,
  SearchField,
  SearchInput,
  Image
} from './Navbar.style';

class Navbar extends Component {
  search(event) {
    event.preventDefault();
    PubSub.publish('search', this.searchedBeer.value);
  }

  render() {
    return (
      <Box>
        <Container>
          <Link to="/">
            <Brand>
              <Image alt="Punk API" src={Logo} />
            </Brand>
          </Link>
        </Container>
        <SearchField role="search">
          <SearchInput
            name="search"
            type="text"
            placeholder="Search a beer..."
            ref={input => (this.searchedBeer = input)}
            onChange={this.search.bind(this)}
          />
        </SearchField>
      </Box>
    );
  }
}

export default Navbar;
