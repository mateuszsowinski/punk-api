import React, { Component } from 'react';
import {
  Loader,
  Container,
  BeerImage,
  BeerInfo,
  BeerName,
  BeerTagline,
  Volume,
  Description,
  List,
  FoodPairing,
  ListItem,
  TechnicalInfoTitle,
  TechnicalInfoItems,
  Grid
} from './Beer.style';

class Beer extends Component {
  constructor(props) {
    super();
    this.beerId = props.match.params.id;
    this.state = { beer: '' };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    const requestInfo = {
      method: 'GET',
      headers: new Headers({
        'Content-type': 'application/json'
      })
    };

    fetch('https://api.punkapi.com/v2/beers/' + this.beerId, requestInfo)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(
            `Request to get a beer with id ${this.beerId} failed`
          );
        }
      })
      .then(beer => this.setState({ beer: beer[0] }));
  }

  render() {
    let beer = this.state.beer;
    if (!beer.volume || !beer.food_pairing) {
      return <Loader />;
    }
    return (
      <Container>
        <BeerImage src={beer.image_url} alt={beer.name + 'picture'} />
        <Grid>
          <BeerInfo>
            <BeerName>{beer.name}</BeerName>
            <BeerTagline>{beer.tagline}</BeerTagline>
            <Volume>
              Volume: {beer.volume.value} {beer.volume.unit}
            </Volume>
            <Description>{beer.description}</Description>
            <List>
              <FoodPairing>Food pairing</FoodPairing>
              {beer.food_pairing.map(food => {
                return <ListItem>{food}</ListItem>;
              })}
            </List>
            <TechnicalInfoTitle>
              <TechnicalInfoItems>
                First brewed: {beer.first_brewed}
              </TechnicalInfoItems>
              <TechnicalInfoItems>ABV: {beer.abv}</TechnicalInfoItems>
              <TechnicalInfoItems>IBU: {beer.ibu}</TechnicalInfoItems>
              <TechnicalInfoItems>EBC: {beer.ebc}</TechnicalInfoItems>
              <TechnicalInfoItems>SRM: {beer.srm}</TechnicalInfoItems>
              <TechnicalInfoItems>PH: {beer.ph}</TechnicalInfoItems>
            </TechnicalInfoTitle>
          </BeerInfo>
        </Grid>
      </Container>
    );
  }
}

export default Beer;
