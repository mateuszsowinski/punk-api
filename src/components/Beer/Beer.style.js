import styled from 'styled-components';

export const Loader = styled.div`
  font-size: 10px;
  margin: 50px auto;
  text-indent: -9999em;
  width: 11em;
  height: 11em;
  border-radius: 50%;
  background: #fff;
  background: -moz-linear-gradient(left, #fff 10%, rgba(255, 255, 255, 0) 42%);
  background: -webkit-linear-gradient(
    left,
    #fff 10%,
    rgba(255, 255, 255, 0) 42%
  );
  background: -o-linear-gradient(left, #fff 10%, rgba(255, 255, 255, 0) 42%);
  background: -ms-linear-gradient(left, #fff 10%, rgba(255, 255, 255, 0) 42%);
  background: linear-gradient(to right, #fff 10%, rgba(255, 255, 255, 0) 42%);
  position: relative;
  -webkit-animation: load3 1.4s infinite linear;
  animation: load3 1.4s infinite linear;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);

  &:before {
    width: 50%;
    height: 50%;
    background: #fff;
    border-radius: 100% 0 0 0;
    position: absolute;
    top: 0;
    left: 0;
    content: '';
  }
  &:after {
    background: #0dc5c2;
    width: 75%;
    height: 75%;
    border-radius: 50%;
    content: '';
    margin: auto;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }
`;

export const Container = styled.div`
  display: grid;
  grid-gap: 10px;
  grid-template-columns: auto auto;
  margin: 10px auto 0 auto;
  padding-right: 15px;
  padding-left: 15px;
  background-image: radial-gradient(#666666, #8a8a8a, #b5b5b5, #d7d7d7);
  padding: 30px 50px 30px 50px;
  border-style: solid;
  border-radius: 10px;
`;

export const BeerImage = styled.img`
  width: auto;
  height: 700px;
  position: relative;
  grid-column: 1/2
  float: left;
  padding: 20px 100px 0 100px;
`;

export const BeerInfo = styled.div`
  position: relative;
  grid-column: 2/2;
`;

export const BeerName = styled.div`
  font-size: 40pt;
  font-weight: bold;
  font-style: oblique;
`;

export const BeerTagline = styled.div`
  font-size: 20pt;
  font-style: italic;
  margin-top: 0%;
  font-weight: bold;
`;

export const Volume = styled.div`
  font-weight: 300;
  text-transform: uppercase;
  font-weight: bold;
`;

export const Description = styled.p`
  margin: 5% 0 5% 0;
  font-size: 16pt;
`;

export const List = styled.ul`
  position: relative;
  padding-right: 45px;
  text-align: center;
`;

export const FoodPairing = styled.ul`
  text-transform: uppercase;
  text-align: center;
  font-weight: 600;
  background: -moz-linear-gradient(
    left,
    rgba(204, 51, 0, 0.81) 0%,
    rgba(255, 153, 51, 0.9) 25%,
    rgba(255, 204, 0, 1) 50%,
    rgba(255, 153, 51, 0.9) 75%,
    rgba(204, 51, 0, 0.81) 100%
  );
  background: -webkit-linear-gradient(
    left,
    rgba(204, 51, 0, 0.81) 0%,
    rgba(255, 153, 51, 0.9) 25%,
    rgba(255, 204, 0, 1) 50%,
    rgba(255, 153, 51, 0.9) 75%,
    rgba(204, 51, 0, 0.81) 100%
  );
  background: linear-gradient(
    to right,
    rgba(204, 51, 0, 0.81) 0%,
    rgba(255, 153, 51, 0.9) 25%,
    rgba(255, 204, 0, 1) 50%,
    rgba(255, 153, 51, 0.9) 75%,
    rgba(204, 51, 0, 0.81) 100%
  );
`;

export const ListItem = styled.li`
  position: relative;
  display: block;
  padding: 10px 15px;
  margin-bottom: -1px;
  background-color: #fff;
  border: 1px solid #ddd;
`;

export const TechnicalInfoTitle = styled.div`
  font-weight: 300;
  margin-top: 5%;
`;

export const TechnicalInfoItems = styled.p`
  display: inline;
  margin: 0 2.5rem 0px 0px;
`;

export const Grid = styled.div`
  display: grid;
  grid-gap: 10px;
  width: auto;
  margin-top: 75px;
  margin-right: 100px;
  margin-bottom: auto;
  padding: 30px 50px 30px 50px;
  border-style: solid;
  border-radius: 10px;
  background-color: #fafae7;
  box-shadow: 30px 30px 70px #182019;
`;
