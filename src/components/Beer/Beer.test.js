import React from 'react';
import { shallow } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Beer from './Beer';

configure({ adapter: new Adapter() });

const match = {
  params: {
    id: 1
  }
};

describe('Beer test', () => {
  it('Render without crash', () => {
    const beer = shallow(<Beer match={match} />);
    expect(beer);
  });
});
