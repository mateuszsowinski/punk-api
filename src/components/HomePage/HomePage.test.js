import React from 'react';
import { shallow } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import HomePage from './HomePage';

configure({ adapter: new Adapter() });

describe('HomePage test', () => {
  it('Render without crash', () => {
    const homePage = shallow(<HomePage />);
    expect(homePage);
  });
});
