import React, { Component } from 'react';
import PubSub from 'pubsub-js';
import {
  Image,
  Box,
  Grid,
  Caption,
  BeerName,
  BeerTagline,
  Bold,
  StyledLink,
  Ingredients
} from './HomePage.style';

class HomePage extends Component {
  constructor() {
    super();
    this.allBeers = [];
    this.state = { beers: [] };
  }

  componentDidMount() {
    PubSub.subscribe('search', (topic, value) => {
      this.search(value);
    });
    this.getBeers();
  }

  search(value) {
    var list = this.allBeers.filter(beer => beer.name.includes(value));
    this.setState({ beers: list });
  }

  getBeers() {
    const self = this;
    const requestInfo = {
      method: 'GET',
      headers: new Headers({
        'Content-type': 'application/json'
      })
    };

    fetch('https://api.punkapi.com/v2/beers', requestInfo)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Request to get all beers failed');
        }
      })
      .then(beers => {
        self.allBeers = beers;
        this.setState({ beers: self.allBeers });
      });
  }

  render() {
    return (
      <Box>
        {this.state.beers.map(beer => {
          return (
            <Grid key={beer.name}>
              <StyledLink to={'/beer/' + beer.id}>
                <Image src={beer.image_url} alt={beer.name + ' picture'} />
                <Caption>
                  <BeerName>{beer.name}</BeerName>
                  <BeerTagline>{beer.tagline}</BeerTagline>
                  <Ingredients>
                    Ingredients:
                    <Bold> Malt(s): </Bold>
                    {beer.ingredients.malt.map(malt => {
                      return (
                        malt.name +
                        ': ' +
                        malt.amount.value +
                        ' ' +
                        malt.amount.unit +
                        ', '
                      );
                    })}
                    <Bold>Hop(s): </Bold>
                    {beer.ingredients.hops.map(hop => {
                      return (
                        hop.name +
                        ': ' +
                        hop.amount.value +
                        ' ' +
                        hop.amount.unit +
                        ', '
                      );
                    })}
                    <Bold>Yeast:</Bold> {beer.ingredients.yeast}
                  </Ingredients>
                </Caption>
              </StyledLink>
            </Grid>
          );
        })}
      </Box>
    );
  }
}

export default HomePage;
