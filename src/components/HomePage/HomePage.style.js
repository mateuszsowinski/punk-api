import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Image = styled.img`
  height: 400px;
  width: 200;
`;

export const Box = styled.div`
  display: grid;
  grid-gap: 10px;
  grid-template-columns: auto auto auto;
  align: center;
  margin-top: 10px;
`;

export const Grid = styled.div`
  float: left;
  display: grid;
  grid-gap: 10px;
  width: auto;
  border-style: solid;
  border-radius: 10px;
  background: -moz-linear-gradient(
    left,
    rgba(255, 204, 0, 1) 0%,
    rgba(255, 153, 51, 0.9) 25%,
    rgba(204, 51, 0, 0.81) 50%,
    rgba(255, 153, 51, 0.9) 75%,
    rgba(255, 204, 0, 1) 100%
  );
  background: -webkit-linear-gradient(
    left,
    rgba(255, 204, 0, 1) 0%,
    rgba(255, 153, 51, 0.9) 25%,
    rgba(204, 51, 0, 0.81) 50%,
    rgba(255, 153, 51, 0.9) 75%,
    rgba(255, 204, 0, 1) 100%
  );
  background: linear-gradient(
    to right,
    rgba(255, 204, 0, 1) 0%,
    rgba(255, 153, 51, 0.9) 25%,
    rgba(204, 51, 0, 0.81) 50%,
    rgba(255, 153, 51, 0.9) 75%,
    rgba(255, 204, 0, 1) 100%
  );
`;

export const Caption = styled.div`
  padding-top: 8px;
  padding-bottom: 8px;
  color: #777;
  text-align: left;
`;

export const BeerName = styled.h3`
  font-size: 24pt;
  font-family: 'Jazz LET', fantasy;
  text-align: center;
  color: #303030;
`;

export const BeerTagline = styled.p`
  margin-top: -15px;
  text-align: center;
  font-size: 16pt;
  font-style: italic;
  color: #000000;
  font-weight: bold;
`;

export const Ingredients = styled.div`
  font-size: 12pt;
  text-align: center;
  color: #000000;
`;

export const Bold = styled.b`
  font-weight: bold;
`;

export const StyledLink = styled(Link)`
  text-decoration: none;
`;
