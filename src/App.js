import React, { Component } from 'react';
import Navbar from './components/Navbar';
import { AppBox } from './App.style';

class App extends Component {
  render() {
    return (
      <AppBox>
        <Navbar />
        {this.props.children}
      </AppBox>
    );
  }
}

export default App;
