import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import HomePage from './components/HomePage';
import Beer from './components/Beer';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router, Route } from 'react-router-dom';

ReactDOM.render(
  <Router>
    <App>
      <Route exact path="/" component={HomePage} />
      <Route path="/beer/:id" component={Beer} />
    </App>
  </Router>,
  document.getElementById('root')
);
registerServiceWorker();
