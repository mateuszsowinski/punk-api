import React from 'react';
import { shallow } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';

configure({ adapter: new Adapter() });

describe('App test', () => {
  it('Render without crash', () => {
    const app = shallow(<App />);
    expect(app);
  });
});
